# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/anthony/fixed_cpp_sim/install/include".split(';') if "/home/anthony/fixed_cpp_sim/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "rospy;actionlib;interactive_markers;std_msgs;sensor_msgs;geometry_msgs;nav_msgs;visualization_msgs;actionlib_msgs;turtlebot3_msgs;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "turtlebot3_example"
PROJECT_SPACE_DIR = "/home/anthony/fixed_cpp_sim/install"
PROJECT_VERSION = "1.2.0"
