# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/anthony/fixed_cpp_sim/src/turtlebot3/turtlebot3_slam/include".split(';') if "/home/anthony/fixed_cpp_sim/src/turtlebot3/turtlebot3_slam/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;sensor_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "turtlebot3_slam"
PROJECT_SPACE_DIR = "/home/anthony/fixed_cpp_sim/devel"
PROJECT_VERSION = "1.2.0"
